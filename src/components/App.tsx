import { mixins } from 'vue-class-component'
import { Component, Vue } from 'vue-property-decorator'

@Component({
  components: {},
  props: {}
})
export default class AppComponent extends mixins(Vue) {
  constructor () {
    super()
  }
}
