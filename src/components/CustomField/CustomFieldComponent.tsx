/*
Component Name: CustomField
Author: Nick Dam
Date: Tue Feb 09 2021 10:26:10 GMT+1100 (Australian Eastern Daylight Time)
*/
import { mixins } from 'vue-class-component'
import { Component, Vue } from 'vue-property-decorator'

@Component({
  components: {},
  props: {}
})
export default class CustomFieldComponent extends mixins(Vue) {
  constructor () {
    super()
  }
}
